package dev.hack5.restartril

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.ServiceState
import android.telephony.ServiceState.*
import android.telephony.TelephonyCallback
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.getSystemService
import dev.hack5.restartril.RILRestarter.restartRIL

@SuppressLint("StaticFieldLeak") // acceptable because these callbacks should last the entire lifecycle of the application and only store a reference to the application context
class BootReceiver : BroadcastReceiver() {

    @Deprecated("Deprecated in Java")
    @Suppress("DEPRECATION", "DeprecatedCallableAddReplaceWith")
    object OldCallback : android.telephony.PhoneStateListener() {
        var telephonyManager: TelephonyManager? = null
        var context: Context? = null

        @Deprecated("Deprecated in Java")
        override fun onServiceStateChanged(serviceState: ServiceState?) {
            notifyServiceStateChanged(serviceState, telephonyManager, context)
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    object NewCallback : TelephonyCallback(), TelephonyCallback.ServiceStateListener {
        var telephonyManager: TelephonyManager? = null
        var context: Context? = null

        override fun onServiceStateChanged(serviceState: ServiceState) {
            notifyServiceStateChanged(serviceState, telephonyManager, context)
        }
    }

    companion object {
        @SuppressLint("HardwareIds")
        fun notifyServiceStateChanged(serviceState: ServiceState?, telephonyManager: TelephonyManager?, context: Context?) {
            if (telephonyManager == null) {
                Log.e(tag, "Telephony manager is null")
                return
            }
            if (context == null) {
                Log.e(tag, "Context is null")
                return
            }

            val mightNeedRestart = when (serviceState?.state) {
                STATE_EMERGENCY_ONLY -> false
                STATE_IN_SERVICE -> false
                STATE_OUT_OF_SERVICE -> true
                STATE_POWER_OFF -> true
                else -> {
                    Log.e(tag, "Unknown state ${serviceState?.state} $serviceState")
                    true
                }
            }
            Log.v(tag, "mightNeedRestart = $mightNeedRestart")

            if (mightNeedRestart) {
                if (telephonyManager.subscriberId == null) {
                    Log.v(tag, "Null subscriber ID, restarting RIL")
                    context.restartRIL {
                        Toast.makeText(context, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

        fun registerListener(context: Context) {
            context.ensurePermissions {
                val telephonyManager = context.getSystemService<TelephonyManager>() ?: run {
                    Log.e(tag, "No telephony manager")
                    return@ensurePermissions
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    NewCallback.telephonyManager = telephonyManager
                    NewCallback.context = context.applicationContext
                    telephonyManager.registerTelephonyCallback(
                        Runnable::run,
                        NewCallback
                    )
                } else {
                    @Suppress("DEPRECATION")
                    OldCallback.telephonyManager = telephonyManager
                    @Suppress("DEPRECATION")
                    OldCallback.context = context.applicationContext
                    @Suppress("DEPRECATION")
                    telephonyManager.listen(OldCallback, android.telephony.PhoneStateListener.LISTEN_SERVICE_STATE)
                }
            }
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != Intent.ACTION_BOOT_COMPLETED) {
            Log.e(tag, "Bad broadcast received")
            return
        }

        registerListener(context)
    }
}


private const val tag = "RestartRIL.BootReceiver"