package dev.hack5.restartril

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.topjohnwu.superuser.Shell


object RILRestarter {
    private var lastRestart = 0L
    var sharedPreferences: SharedPreferences? = null

    @SuppressLint("ApplySharedPref")
    fun Context.restartRIL(callback: (String) -> Unit) {
        val start = System.currentTimeMillis()
        if (sharedPreferences == null) {
            sharedPreferences = getSharedPreferences("prefs", Context.MODE_PRIVATE)
            lastRestart = sharedPreferences!!.getLong("lastRestart", 0L)
        }
        val gap = start - lastRestart
        Log.v(tag, "gap=$gap")
        if (gap < 30_000L) {
            Log.v(tag, "Last restart was $gap ms ago")
            return
        }
        lastRestart = start
        sharedPreferences!!.edit().putLong("lastRestart", lastRestart).commit()


        Root.getShell { sh ->
            Shell.cmd(
                //"setenforce 0",
                "setprop persist.sys.phh.restart_ril false",
                "pkill -u radio",
                "pkill -u com.android.phone",
                "pkill -u com.mediatek.ims",
                "setprop persist.sys.phh.restart_ril true"
            ).submit {
                callback(
                    getString(
                        R.string.ril_restarted,
                        sh.status,
                        it.code,
                        it.err.joinToString("\n"),
                        it.out.joinToString("\n")
                    )
                )
            }
        }
    }
}

private const val tag = "RILRestarter"