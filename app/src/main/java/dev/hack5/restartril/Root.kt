package dev.hack5.restartril

import com.topjohnwu.superuser.Shell

object Root {
    init {
        Shell.enableVerboseLogging = BuildConfig.DEBUG
        Shell.setDefaultBuilder(Shell.Builder.create().setTimeout(5000))
    }

    fun getShell(callback: Shell.GetShellCallback) = Shell.getShell(callback)
}