package dev.hack5.restartril

import android.os.Bundle
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import dev.hack5.restartril.BootReceiver.Companion.registerListener
import dev.hack5.restartril.RILRestarter.restartRIL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                finish()
            }
        })
    }

    override fun onStart() {
        super.onStart()

        registerListener(applicationContext)

        restartRIL {
            runOnUiThread {
                val v = TextView(this)
                v.text = it
                setContentView(v)
            }
        }
    }
}