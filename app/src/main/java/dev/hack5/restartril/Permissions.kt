package dev.hack5.restartril

import android.app.AppOpsManager
import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PackageInfoFlags
import android.content.pm.PermissionInfo
import android.os.Build
import com.topjohnwu.superuser.Shell

fun Context.ensurePermissions(callback: (String) -> Unit) {
    Root.getShell { sh ->
        val packageManger = packageManager
        val permissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            packageManger.getPackageInfo(packageName, PackageInfoFlags.of(PackageManager.GET_PERMISSIONS.toLong()))
        } else {
            @Suppress("DEPRECATION")
            packageManger.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS)
        }.requestedPermissions ?: emptyArray()
        Shell.cmd(
            *permissions.flatMap { listOfNotNull("pm grant $packageName $it", AppOpsManager.permissionToOp(it)?.let { appOp -> "appops set $packageName $appOp allow"}) }.plus("appops set $packageName android:read_device_identifiers allow").toTypedArray()
        ).submit {
            callback(
                getString(
                    R.string.permissions_granted,
                    sh.status,
                    it.code,
                    it.err.joinToString("\n"),
                    it.out.joinToString("\n")
                )
            )
        }
    }
}